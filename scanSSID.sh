#!/bin/bash

SERVER_URL="http://35.237.59.96:8080/"
SSID_REGEX="9300-*"
INTERFACE="wlp58s0"
NORMAL_NETWORK="ecfd0ab4-8e3f-4ac5-aa45-83a2a124ce75" # with nmcli c show

drone=""

while true
do
    #nmcli dev wifi rescan
    NETWORKS="$(nmcli -g SSID,SIGNAL dev wifi)"

    drone_found=False

    while read -r network; do
        network_name="$(cut -d':' -f1 <<< "$network")"
        network_strength="$(cut -d':' -f2 <<< "$network")"
        
        if [[ "$network_name" =~ $SSID_REGEX ]]; then
            # DRONE FOUND --> Connect to WIFI
            echo "Found network: $network_name with strength $network_strength"

            echo "Connecting..."
            nmcli dev wifi connect "$network_name"
            echo "Connected to drone network: $network_name"
            MAC_ADRESS=$(iwconfig $INTERFACE | grep "Access Point" | awk '{print $6}')

            # Reconnect
            nmcli c up uuid "$NORMAL_NETWORK"

            # Send the info to the server
            drone=$(curl -X POST "$SERVER_URL/drones" -H "accept: application/ld+json" -H "Content-Type: application/ld+json" -d "{ \"macAdress\": \"$MAC_ADRESS\", \"ssid\": \"$network_name\"}")
            curl -X POST "$SERVER_URL/drone_sightings" -H "accept: application/ld+json" -H "Content-Type: application/ld+json" -d "{ \"drone\": \"$drone\"}"

            # Wait till the signal dies

            #curl -d "drone=$drone_name&param2=value2" -X POST "$SERVER_URL/drone_sightings""
        fi
        
    done <<< "$NETWORKS"
done
