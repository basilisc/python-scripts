import math
import csv
import statistics

def bisect(sensorSpread, s1Distance, s2Distance):
   cosa = ( math.pow(sensorSpread,2) + math.pow(s1Distance,2) - math.pow(s2Distance,2) ) / ( 2.0 * sensorSpread * s1Distance )
   a = math.acos(cosa)
   cosb = ( math.pow(sensorSpread,2) - math.pow(s1Distance,2) + math.pow(s2Distance,2) ) / ( 2.0 * sensorSpread * s2Distance )
   b = math.acos(cosb)

   distance = math.sin(a) * s1Distance 
   #print("Drone is " + str(distance) + " meters away!")

   micSpread = sensorSpread/2.0
   micDistancesquared = math.pow(micSpread,2) + math.pow(s1Distance,2) - 2.0*micSpread*s1Distance*cosa
   micDistance = math.sqrt(micDistancesquared)

   cosy = ( math.pow(micSpread,2) - math.pow(s1Distance,2) + math.pow(micDistance,2) ) / ( 2.0 * micSpread * micDistance )
   y = math.acos(cosy)
   #print("Drone angle: " + str(math.degrees(y)))
   return math.degrees(y)

def calcDistance(Tx, Rx, f):
   logd = (Tx - Rx + 27.55 - 20.0*math.log10(f)) / 20.0
   d = math.pow(10.0, logd) 
   #print("distance calculated: " + str(d))
   return d

def parseDict(ifdict):
   iwdict = {}
   for iface in ifdict:
      iwdict[iface] = statistics.median(ifdict[iface])
   return iwdict

def wrapper(vals, Tx = 18, f = 2417, baseline = 2.5):
   iwdict = parseDict(vals)
   d = []
   for interface in iwdict:
      Rx = iwdict[interface]
      #d.append(calcDistance(Tx, Rx, f))
      d.append(Rx)
   if len(d) != 2:
      raise ValueError("2 interfaces needed")
   return bisect(-30, d[0], d[1])

if __name__ == '__main__':
   Tx = 18
   f = 2417
   basedistance = 2.5
   val = {"mon0":[-28,-29,-30,-32],"mon1":[-14,-13,-12,-10]}
   
   iwdict = parseDict(val)
   Rx1 = iwdict['mon0']
   Rx2 = iwdict['mon1']

   d1 = calcDistance(Tx, Rx1, f)
   d2 = calcDistance(Tx, Rx2, f)

   print(bisect(basedistance, d1, d2))
