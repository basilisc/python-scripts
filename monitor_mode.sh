#!/bin/bash

set -e

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if [[ "$#" = 1 ]] ; then
    echo "Too little arguments"
    exit
fi

if [[ ! -f /etc/network/interfaces.bak ]] ; then
    cp /etc/network/interfaces /etc/network/interfaces.bak
fi

#card=$(iw dev | grep -P "Interface" | sed -e "s#\tInterface ##")


if [[ "$2" = "on" ]] ; then
    #ifconfig $1 down
    #iwconfig $1 mode monitor
    #if [[ -n "$3" ]] ; then
        #iwconfig $1 channel $3
    #fi
    #ifconfig $1 up
    #printf "iface %s inet manual\niface %s inet6 manual\n" "$1" "$1" >> /etc/network/interfaces
    service network-manager stop
    service wpa_supplicant stop
    airmon-ng start $1
    service network-manager start
    service wpa_supplicant start
    exit
fi

if [[ "$2" = "off" ]] ; then
   #ifconfig $1 down
   #iwconfig $1 mode managed
   #ifconfig $1 up
   #cp /etc/network/interfaces.bak /etc/network/interfaces
   airmon-ng stop mon0
   airmon-ng stop mon1
   airmon-ng stop $1
   service network-manager restart
   service wpa_supplicant restart
fi
