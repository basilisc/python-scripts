#!/usr/bin/python3

import re
import sys
import time
import datetime
import pyshark
import threading
from itertools import filterfalse
import code
from statistics import median
import requests
import biangle
from matplotlib import pyplot as plt
import numpy as np

def getFromApi(mac):
 #r = requests.get("http://35.185.88.197:8080/drones")
 #if r.status_code != 200:
 # print('Get (%s) failed with %d' % (mac, r.status_code))
 # return
 cache = Cache.getInstance()
 cache.Lock()
 #for drone in r.json():
 # cache.registerMac(drone['macAdress'], drone['id'])
 if re.match('^00:24:1', mac) is not None:
 #if re.match('00:24:1e:80:50:93', mac) is not None:
  cache.registerMac(mac, 0)
 cache.Unlock()

def pushToApi(mac, timestamp, signal, phi):
  '''timestamp = datetime.datetime.fromtimestamp(timestamp).strftime('%Y-%m-%dT%H:%M:%S.%fZ')
 r = requests.post("http://35.185.88.197:8080/dronePath", data = {
  'date': timestamp,
  'droneSighting': '',
  'signalStrength': signal,
  'direction': phi})
 if r.status_code != 201 or True:'''
  #print('Post (%s, %f) failed with %d' % (mac, phi, r.status_code))
  print('%s %s' % (mac, phi))
  phi_rad = phi * 3.141592 / 180
  cache = Cache.getInstance()
  cache.Lock()
  cache.phis = np.append(cache.phis, phi_rad)
  cache.rs = np.append(cache.rs, 1)
  cache.Unlock()
  return



class Cache(object):
 instance = []
 def __init__(self):
  self.lock = threading.Lock()
  self.lock.acquire()
  self.macs = {}
  self.ignored_macs = []
  self.phis = np.zeros(0,)
  self.rs = np.zeros(0,)
  Cache.instance.append(self)
  self.lock.release()

 @staticmethod
 def getInstance():
  if len(Cache.instance) == 0:
   Cache.instance.append(Cache())
  return Cache.instance[0]

 def Lock(self):
  self.lock.acquire()

 def Unlock(self):
  self.lock.release()

 def registerMac(self, mac, api_id):
  if mac in self.ignored_macs: self.ignored_macs.remove(mac)
  self.macs[mac] = Mac(mac, api_id)

 def appendToMac(self, mac, interface, dBm, timestamp):
  self.macs[mac].append(interface, dBm, timestamp);

 def checkMac(self, mac):
  if mac in self.ignored_macs:
   #print('ignored %s' % mac)
   return False
  if mac in self.macs:
   #print('known %s' % mac)
   return True
  self.ignored_macs.append(mac)
  #print('ignoring %s' % mac)
  threading.Thread(target=getFromApi, args=(mac,)).start()
  return False

 def flush_dBm(self):
  for mac in self.macs:
   self.macs[mac].flush()

class Mac(object):
 def __init__(self, mac, api_id):
  self.mac = mac
  self.api_id = api_id
  self.interfaces = {}
  self.api_timestamp = time.time()
  print('Registered %s' % mac)

 def append(self, interface, dBm, timestamp):
  if interface not in self.interfaces:
    self.interfaces[interface] = []
  self.interfaces[interface].append(dBm)

 def flush(self):
   #print('flushing %s' % self.mac)
   if len(self.interfaces) == 0:
    return
   for key in self.interfaces:
    if len(self.interfaces[key]) == 0:
     return

   #print('biangle %s' % self.mac)
   means = []
   for key in self.interfaces:
     means.append(median(self.interfaces[key]))
   signal = median(means)
   try:
    phi = biangle.wrapper(self.interfaces);
    threading.Thread(target=pushToApi, args=(self.mac, time.time(), signal, 3./2*(int(phi)-90))).start()
   except ValueError as e:
    #print('math domain error!')
    pass

   for key in self.interfaces:
    self.interfaces[key] = []


def main(interfaces):
 if len(interfaces) == 0:
  print("Usage: ./sniff.py mon0 mon1 mon2 ... monN")
  return

 cache = Cache.getInstance()

 threading.Thread(target=eater_thread).start()

 threads = []
 for interface in interfaces:
  th = threading.Thread(target=capture_thread, args=(interface,))
  th.start()
  threads.append(th)
 fig = plt.figure()
 ax = fig.add_subplot(111, projection='polar')
 plt.show(block=False)
 while(True):
  for th in threads:
   if not th.is_alive():
    return
  time.sleep(2)
  plt.ion()
  cache.Lock()
  
  colors = np.arange(cache.phis.size) / cache.phis.size
  c = ax.scatter(cache.phis, cache.rs, c=colors)
  fig.canvas.draw()
  cache.Unlock()

def capture_thread(interface):
 print("Starting capture on %s" % interface)
 capture = pyshark.LiveCapture(interface=interface)
 cache = Cache.getInstance()
 
 def closure(pkt):
  cache.Lock()
  try:
   mac = pkt['wlan'].sa
   dBm = pkt['wlan_radio'].signal_dbm
   timestamp = pkt.frame_info.time_epoch
   if cache.checkMac(mac):
    cache.appendToMac(str(mac), str(interface), int(dBm), float(timestamp))

  except AttributeError as e:
   pass
  finally:
   cache.Unlock()

 capture.apply_on_packets(closure)

def eater_thread():
 time.sleep(2.5)
 cache = Cache.getInstance()
 while(True):
  time.sleep(3)
  cache.Lock()
  cache.flush_dBm()
  cache.Unlock()

'''
class Knowledge(object):
 SSID_REGEX = "9300-[\dA-F]{6}"
 SSID_ENTRY_TIMEOUT = 60 * 5

 def __init__(self, interface):
  self.drone_ssids = []
  self.nr = 0
  self.interface = interface
  self.output = open("%s_%d.csv" % (interface, int(time.time())), 'wb')
  self.output.write("timestamp drone_nr power_dbm ssid_to_int10\n".encode('utf-8'))

 def __del__(self):
  self.output.close()

 def print(self, entry, epoch, power, cached = ""):
  ssid = int(entry['ssid'][5::],16)
  self.output.write(('%s %d %s %d\n' % (epoch, entry['nr'], str(power), ssid)).encode('utf-8'))

 def parse_packet(self, pkt):
  self.clear_list()

  #if 'ip' in pkt and pkt['ip'].src != '172.16.10.1' and pkt['ip'].dst != '172.16.10.1':
   #return
  #code.interact(local=locals())

  if 'wlan' not in pkt:
   return

  #get mac from beacon
  if hasattr(pkt['wlan'], 'sa'):
   mac_sa = pkt['wlan'].sa
   for i, entry in enumerate(self.drone_ssids):
    if entry['mac'] == mac_sa:
     self.drone_ssids[i]['time'] = time.time()
     self.print(entry, pkt.frame_info.time_epoch, pkt['wlan_radio'].signal_dbm, "(cached)")
     return
 
  if 'wlan_mgt' not in pkt or not hasattr(pkt['wlan_mgt'], 'ssid') or not hasattr(pkt['wlan'], 'sa'):
   return
  if int(pkt['wlan'].fc_subtype) != 8:
    return
  mac = pkt['wlan'].sa
  ssid = pkt['wlan_mgt'].ssid
  match = re.match(self.SSID_REGEX, ssid)
  if match is None:
   return
  print("Drone SSID detected: %s, MAC SA: %s, %d, %s" % (ssid, mac, self.nr, self.interface))
  entry = {
   'ssid': ssid,
   'time': time.time(),
   'mac': mac,
   'nr': self.nr
  }
  self.drone_ssids.append(entry)
  self.nr += 1
  self.print(entry, pkt.frame_info.time_epoch, pkt['wlan_radio'].signal_dbm)
 
 def clear_list(self):
  max_time = time.time() + self.SSID_ENTRY_TIMEOUT
  def outdated(entry):
   return entry['time'] > max_time
  len_before = len(self.drone_ssids)
  self.drone_ssids[:] = filterfalse(outdated, self.drone_ssids[:])
  len_after = len(self.drone_ssids)
  if not len_before == len_after:
   print("Removed %d", len_before - len_after)
'''
if __name__ == "__main__":
 main(sys.argv[1::])
